package com.tsystems.javaschool.tasks.pyramid;

import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {

		int i, j;
		int currentInputNumberIndex;
		int pyramidLevelsCount = 0;
		int levelElementsCount; // The elements from inputNumbers count. Without additional zeroes on the level
		int pyramidLowestlevelElementsCount = 0;
		int pyramidWidth = 0;
		int pyramidElementIndex=0;
		int oneSideZeroesCount;
		int currentLowestNumber, currentNumber;
		
		int inputNumbersLength = inputNumbers.size();
		
		final int MAX_INPUT_NUMBERS_COUNT = 1000000000;
		
		if( inputNumbersLength > MAX_INPUT_NUMBERS_COUNT ){
			throw new CannotBuildPyramidException();
		}

		int[] inputNumbersArray = new int[ inputNumbersLength ];
		int[][] pyramid;

		// Getting the 1D array from the list
		//
		for( i = 0; i < inputNumbersLength; i++ ) {
			if( inputNumbers.get( i ) == null ){
				throw new CannotBuildPyramidException();
			}
			else {
				inputNumbersArray[ i ] = inputNumbers.get( i );
			}
		}

		// Sorting 1D array ascending
		//
		for( i = 0; i < inputNumbersLength; i++ ) {
			currentLowestNumber = inputNumbersArray[ i ];
			for( j=i+1; j < inputNumbersLength; j++ ){
				currentNumber = inputNumbersArray[ j ];
				if( currentLowestNumber > currentNumber ){
					inputNumbersArray[ i ] = currentNumber;
					inputNumbersArray[ j ] = currentLowestNumber;
					currentLowestNumber = currentNumber;		
				}
			}
		}
				
		
		// Pyramid levels counting and construction possibility checking
		//
		i = 0;
		levelElementsCount = 1;
		while( (inputNumbersLength - i) >= levelElementsCount ) {
			
			i += levelElementsCount;
			levelElementsCount += 1;
			pyramidLevelsCount++;

			pyramidLowestlevelElementsCount = levelElementsCount - 1;
			pyramidWidth = 2 * pyramidLowestlevelElementsCount - 1;
			
			if( ( (inputNumbersLength - i) < levelElementsCount) && ( (inputNumbersLength - i) > 0) ){
				throw new CannotBuildPyramidException();
			}
		}
		
		// Constructing the pyramid
		//
		pyramid = new int[ pyramidLevelsCount ][ pyramidWidth ];
			
		levelElementsCount = 1;
		currentInputNumberIndex = 0;
			
		for( i = 0; i < pyramidLevelsCount; i++ ){
			pyramidElementIndex = 0;
			oneSideZeroesCount = ( pyramidWidth - ( levelElementsCount * 2 - 1 ) ) / 2;
			for( j = 0; j < oneSideZeroesCount; j++ ){
				pyramid[ i ][ pyramidElementIndex ] = 0;
				pyramidElementIndex++;
			}
			pyramid[ i ][ pyramidElementIndex ] = inputNumbersArray[ currentInputNumberIndex ];
			currentInputNumberIndex++;
			pyramidElementIndex++;
				
			for( j = 0; j <  (levelElementsCount - 1); j++ ) {
				pyramid[ i ][ pyramidElementIndex ] = 0;
				pyramidElementIndex++;
				pyramid[ i ][ pyramidElementIndex ] = inputNumbersArray[ currentInputNumberIndex ];
				pyramidElementIndex++;
				currentInputNumberIndex++;
			}
				
			for( j = 0; j < oneSideZeroesCount; j++ ){
				pyramid[ i ][ pyramidElementIndex ] = 0;
				pyramidElementIndex++;
			}
			levelElementsCount++;
		}

        return pyramid;
    }


}
